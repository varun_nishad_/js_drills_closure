function cbFun(...args) {
  return `value-${[...args].toString()}`
}

function cacheFunction(cb) {
  let cacheObj = {}

  return function invoke(...args) {

    if (cacheObj.hasOwnProperty([...args].toString())) {
      console.log("This is from object");
      return cacheObj[[...args].toString()];
    }

    cacheObj[[...args].toString()] = cb(...args)
    return cacheObj[[...args].toString()]
  }
}

export {
  cacheFunction,
  cbFun
}
