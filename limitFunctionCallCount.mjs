function cbFun(count) {
  return `${count} time`
}

function limitFunctionCallCount(cb, n) {

  let count = 0

  return function inner() {
    count += 1
    if (count > n) {
      return null;
    }
    return cb(count);
  }
}

export { cbFun,limitFunctionCallCount }
