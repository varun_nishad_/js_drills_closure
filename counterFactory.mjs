export function counterFactory() {
  let obj = {}
  let count= 0;
  function increment(){
    count++;
    return count;
  }
  function decrement(){
    count--;
    return count;
  }
    obj = {increment: increment, decrement: decrement}
  return obj;
}
